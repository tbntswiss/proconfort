<?php

/*
|--------------------------------------------------------------------------
| IDs management
|--------------------------------------------------------------------------
|
*/

// --------------- shipping zones ---------------
// --- must reflect db values
$zone1_id = 2;
$zone2_id = 3;
$zone3_id = 4;
$zone_france_id = 10;
$zone_suisse_id = 11;
$zone_usa_id = 12;

// --------------- taxables zones ---------------
$zones_taxable = [
    $zone1_id,
    $zone2_id,
    $zone3_id,
    $zone_france_id
];

// --------------- products ---------------
$couverture_hiver_variants = [
    319,
    320,
    321,
    322,
    323,
    324,
    325,
    326,
];

$couverture_polaire_variants = [
    339,
    340,
    341,
    342,
    343,
    344,
    345,
    346,
];

$couverture_ete_variants = [
    330,
    331,
    332,
    333,
    334,
    335,
    336,
];

$topaze_4_variants = [
    671,
    673,
    674,
    675,
    676,
    677,
];

$topaze_variants = [
    293,
    294,
    295,
    296,
    297,
    298,
    299,
    300,
    301,
    302,
    303,
    304,
];

$tapis_selle_variants = [
    349,
    350,
];

$tenture_variants = [
    362,
    363,
];

$porte_box_variants = [
    279,
    280,
];

$porte_tapis_variants = [
    358,
    359,
];

$panier_xs_variants = [
    353,
];

$panier_s_variants = [
    355,
];

$panier_m_variants = [
    356,
];

$couvre_rein_variants = [
    696,
    727,
    728,
    729,
    730,
    732,
    733,
    734,
    735,
    736,
    737,
    738,
];



/*
|--------------------------------------------------------------------------
| Pricing management
|--------------------------------------------------------------------------
|
*/

// --------------- couverture hiver ---------------
// colissimo
$couverture_hiver_colissimo = [
    $zone_france_id => 16.46,
    $zone1_id       => 21.50,
    $zone2_id       => 25.90,
    $zone3_id       => 33.00,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 33.00,
];

// chronopost
$couverture_hiver_chronopost = [
    $zone_france_id => 21.38,
    $zone1_id       => 34.60,
    $zone2_id       => 38.52,
    $zone3_id       => 45.60,
    $zone_usa_id    => 144.45,
    // $zone_suisse_id => 0,
];

// --------------- couverture polaire ---------------
// colissimo
$couverture_polaire_colissimo = [
    $zone_france_id => 16.46,
    $zone1_id       => 21.50,
    $zone2_id       => 25.90,
    $zone3_id       => 33.00,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 33.00,
];

// chronopost
$couverture_polaire_chronopost = [
    $zone_france_id => 21.38,
    $zone1_id       => 34.60,
    $zone2_id       => 38.52,
    $zone3_id       => 45.60,
    $zone_usa_id    => 144.45,
    // $zone_suisse_id => 0,
];

// --------------- couverture eté ---------------
// colissimo
$couverture_ete_colissimo = [
    $zone_france_id => 16.46,
    $zone1_id       => 21.50,
    $zone2_id       => 25.90,
    $zone3_id       => 33.00,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 33.00,
];

// chronopost
$couverture_ete_chronopost = [
    $zone_france_id => 21.38,
    $zone1_id       => 34.60,
    $zone2_id       => 38.52,
    $zone3_id       => 45.60,
    $zone_usa_id    => 144.45,
    // $zone_suisse_id => 0,
];

// --------------- topaze 4 ---------------
// colissimo
$topaze_4_colissimo = [
    $zone_france_id => 16.46,
    $zone1_id       => 21.50,
    $zone2_id       => 25.90,
    $zone3_id       => 33.00,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 33.00,
];

// chronopost
$topaze_4_chronopost = [
    $zone_france_id => 21.38,
    $zone1_id       => 34.60,
    $zone2_id       => 38.52,
    $zone3_id       => 45.60,
    $zone_usa_id    => 144.45,
    // $zone_suisse_id => 0,
];

// --------------- topaze ---------------
// colissimo
$topaze_colissimo = [
    $zone_france_id => 12.25,
    $zone1_id       => 15.00,
    $zone2_id       => 17.95,
    $zone3_id       => 18.65,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 18.65,
];

// chronopost
$topaze_chronopost = [
    $zone_france_id => 13.50,
    $zone1_id       => 28.50,
    $zone2_id       => 33.00,
    $zone3_id       => 39.64,
    $zone_usa_id    => 75.27,
    // $zone_suisse_id => 0,
];

// --------------- tapis de selle ---------------
// colissimo
$tapis_selle_colissimo = [
    $zone_france_id => 12.25,
    $zone1_id       => 15.00,
    $zone2_id       => 17.95,
    $zone3_id       => 18.65,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 18.65,
];

// chronopost
$tapis_selle_chronopost = [
    $zone_france_id => 13.50,
    $zone1_id       => 28.50,
    $zone2_id       => 33.00,
    $zone3_id       => 39.64,
    $zone_usa_id    => 75.27,
    // $zone_suisse_id => 0,
];

// --------------- tenture ---------------
// colissimo
$tenture_colissimo = [
    $zone_france_id => 12.25,
    $zone1_id       => 15.00,
    $zone2_id       => 17.95,
    $zone3_id       => 18.65,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 18.65,
];

// chronopost
$tenture_chronopost = [
    $zone_france_id => 13.50,
    $zone1_id       => 28.50,
    $zone2_id       => 33.00,
    $zone3_id       => 39.64,
    $zone_usa_id    => 75.27,
    // $zone_suisse_id => 0,
];

// --------------- porte de box ---------------
// colissimo
$porte_box_colissimo = [
    $zone_france_id => 12.25,
    $zone1_id       => 15.00,
    $zone2_id       => 17.95,
    $zone3_id       => 18.65,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 18.65,
];

// chronopost
$porte_box_chronopost = [
    $zone_france_id => 13.50,
    $zone1_id       => 28.50,
    $zone2_id       => 33.00,
    $zone3_id       => 39.64,
    $zone_usa_id    => 75.27,
    // $zone_suisse_id => 0,
];

// --------------- porte tapis de selle ---------------
// colissimo
$porte_tapis_colissimo = [
    $zone_france_id => 12.25,
    $zone1_id       => 15.00,
    $zone2_id       => 17.95,
    $zone3_id       => 18.65,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 18.65,
];

// chronopost
$porte_tapis_chronopost = [
    $zone_france_id => 13.50,
    $zone1_id       => 28.50,
    $zone2_id       => 33.00,
    $zone3_id       => 39.64,
    $zone_usa_id    => 75.27,
    // $zone_suisse_id => 0,
];

// --------------- panier pour chien xs ---------------
// colissimo
$panier_xs_colissimo = [
    $zone_france_id => 12.25,
    $zone1_id       => 15.00,
    $zone2_id       => 17.95,
    $zone3_id       => 18.65,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 18.65,
];

// chronopost
$panier_xs_chronopost = [
    $zone_france_id => 13.50,
    $zone1_id       => 28.50,
    $zone2_id       => 33.00,
    $zone3_id       => 39.64,
    $zone_usa_id    => 75.27,
    // $zone_suisse_id => 0,
];

// --------------- panier pour chien s ---------------
// colissimo
$panier_s_colissimo = [
    $zone_france_id => 13.42,
    $zone1_id       => 16.00,
    $zone2_id       => 19.00,
    $zone3_id       => 20.00,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 20.00,
];

// chronopost
$panier_s_chronopost = [
    $zone_france_id => 14.45,
    $zone1_id       => 30.50,
    $zone2_id       => 34.60,
    $zone3_id       => 41.45,
    $zone_usa_id    => 96.59,
    // $zone_suisse_id => 0,
];

// --------------- panier pour chien m ---------------
// colissimo
$panier_m_colissimo = [
    $zone_france_id => 13.42,
    $zone1_id       => 16.00,
    $zone2_id       => 19.00,
    $zone3_id       => 20.00,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 20.00,
];

// chronopost
$panier_m_chronopost = [
    $zone_france_id => 14.45,
    $zone1_id       => 30.50,
    $zone2_id       => 34.60,
    $zone3_id       => 41.45,
    $zone_usa_id    => 96.59,
    // $zone_suisse_id => 0,
];

// --------------- couvre rein ---------------
// colissimo
$couvre_rein_colissimo = [
    $zone_france_id => 13.42,
    $zone1_id       => 16.00,
    $zone2_id       => 19.00,
    $zone3_id       => 20.00,
    // $zone_usa_id    => 0,
    $zone_suisse_id => 20.00,
];

// chronopost
$couvre_rein_chronopost = [
    $zone_france_id => 14.45,
    $zone1_id       => 30.50,
    $zone2_id       => 34.60,
    $zone3_id       => 41.45,
    $zone_usa_id    => 96.59,
    // $zone_suisse_id => 0,
];



/*
|--------------------------------------------------------------------------
| Price mapping
|--------------------------------------------------------------------------
|
*/

// Holds the whole price mapping for every single product variant
// Mapping structure:
// [<purchasableId> => ['<shippingMethodName>' => [<zoneId> => <shippingFee>]]]

$products = [];

// --------------- couverture hiver ---------------
foreach ($couverture_hiver_variants as $couverture_id) {
    $products[$couverture_id] = [
        'colissimo'  => $couverture_hiver_colissimo,
        'chronopost' => $couverture_hiver_chronopost,
    ];
}

// --------------- couverture polaire ---------------
foreach ($couverture_polaire_variants as $couverture_id) {
    $products[$couverture_id] = [
        'colissimo'  => $couverture_polaire_colissimo,
        'chronopost' => $couverture_polaire_chronopost,
    ];
}

// --------------- couverture ete ---------------
foreach ($couverture_ete_variants as $couverture_id) {
    $products[$couverture_id] = [
        'colissimo'  => $couverture_ete_colissimo,
        'chronopost' => $couverture_ete_chronopost,
    ];
}

// --------------- topaze 4 ---------------
foreach ($topaze_4_variants as $topaze_id) {
    $products[$topaze_id] = [
        'colissimo'  => $topaze_4_colissimo,
        'chronopost' => $topaze_4_chronopost,
    ];
}

// --------------- topaze ---------------
foreach ($topaze_variants as $topaze_id) {
    $products[$topaze_id] = [
        'colissimo'  => $topaze_colissimo,
        'chronopost' => $topaze_chronopost,
    ];
}

// --------------- tapis de selle ---------------
foreach ($tapis_selle_variants as $tapis_id) {
    $products[$tapis_id] = [
        'colissimo'  => $tapis_selle_colissimo,
        'chronopost' => $tapis_selle_chronopost,
    ];
}

// --------------- tenture ---------------
foreach ($tenture_variants as $tenture_id) {
    $products[$tenture_id] = [
        'colissimo'  => $tenture_colissimo,
        'chronopost' => $tenture_chronopost,
    ];
}

// --------------- porte de box ---------------
foreach ($porte_box_variants as $porte_id) {
    $products[$porte_id] = [
        'colissimo'  => $porte_box_colissimo,
        'chronopost' => $porte_box_chronopost,
    ];
}

// --------------- porte tapis de selle ---------------
foreach ($porte_tapis_variants as $porte_id) {
    $products[$porte_id] = [
        'colissimo'  => $porte_tapis_colissimo,
        'chronopost' => $porte_tapis_chronopost,
    ];
}

// --------------- panier pour chien xs ---------------
foreach ($panier_xs_variants as $panier_id) {
    $products[$panier_id] = [
        'colissimo'  => $panier_xs_colissimo,
        'chronopost' => $panier_xs_chronopost,
    ];
}

// --------------- panier pour chien s ---------------
foreach ($panier_s_variants as $panier_id) {
    $products[$panier_id] = [
        'colissimo'  => $panier_s_colissimo,
        'chronopost' => $panier_s_chronopost,
    ];
}

// --------------- panier pour chien m ---------------
foreach ($panier_m_variants as $panier_id) {
    $products[$panier_id] = [
        'colissimo'  => $panier_m_colissimo,
        'chronopost' => $panier_m_chronopost,
    ];
}

// --------------- couvre rein ---------------
foreach ($couvre_rein_variants as $couvre_rein_id) {
    $products[$couvre_rein_id] = [
        'colissimo'  => $couvre_rein_colissimo,
        'chronopost' => $couvre_rein_chronopost,
    ];
}
