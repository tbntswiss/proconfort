<?php
namespace Craft;

class ShippingFees_ShippingMethodsService extends BaseApplicationComponent
{
    private $_products = [];
    private $_zones_taxable = [];

    public function __construct() {
        include 'prices.php';

        $this->_products = $products;
        $this->_zones_taxable = $zones_taxable;
    }

    /**
     * @param Commerce_OrderModel $order
     *
     * @param string $shippingMethod
     *
     * @return float
     */
    public function getShippingFee(\Craft\Commerce_OrderModel $order, $shippingMethod) {
        // ShippingFeesPlugin::vwLog('processing logic for order: ' . $order);
        // TODO update logic right there...

        $shippingZoneId = $this->getShippingZoneIdFromOrder($order);
        $shippingFee = 0;

        foreach ($order->lineItems as $item) {
            $purchasableId = $item->purchasable->id;
            $purchasableShippingPrice = $this->_products[$purchasableId][$shippingMethod][$shippingZoneId] ?? null;

            if ($purchasableShippingPrice === null)
                continue;

            $shippingFee += $item->qty * $purchasableShippingPrice;
        }

        return $shippingFee;
    }

    /**
     * @param Commerce_OrderModel $order
     *
     * @return int
     */
    public function getShippingZoneIdFromOrder(\Craft\Commerce_OrderModel $order) {
        $shippingCountry = $order->shippingAddress->country;
        $shippingZones = craft()->commerce_shippingZones->getAllShippingZones();

        foreach ($shippingZones as $zone) {
            foreach ($zone->countries as $country) {
                // Is the shipping country in the list of zones ?
                if ($country->id === $shippingCountry->id) {
                    return $zone->id;
                }
            }
        }

        return 0;
    }

    /**
     * @param Commerce_OrderModel $order
     *
     * @return bool
     */
    public function methodMatchesOrder(\Craft\Commerce_OrderModel $order, $shippingMethod = '') {
        $shippingZoneId = $this->getShippingZoneIdFromOrder($order);

        foreach ($order->lineItems as $item) {
            $purchasableId = $item->purchasable->id;

            if (isset($this->_products[$purchasableId][$shippingMethod][$shippingZoneId]) === false)
                return false;
        }

        return true;

        // $zonesContainsShippingCountry = false;

        // $shippingCountry = $order->shippingAddress->country;
        // $shippingZones = craft()->commerce_shippingZones->getAllShippingZones();

        // foreach ($shippingZones as $zone) {
        //     foreach ($zone->countries as $country) {
        //         // Is the shipping country in the list of zones ?
        //         if ($country->id === $shippingCountry->id) {
        //             $zonesContainsShippingCountry = true;
        //             break;
        //         }
        //     }
        // }

        // return $zonesContainsShippingCountry;
    }

    /**
     * @param Commerce_OrderModel $order
     *
     * @return bool
     */
    public function isOrderTaxable(\Craft\Commerce_OrderModel $order) {
        $shippingZoneId = $this->getShippingZoneIdFromOrder($order);

        return $this->isZoneTaxable($shippingZoneId);
    }

    /**
     * @param integer $zone_id
     *
     * @return bool
     */
    public function isZoneTaxable($zone_id) {
        return in_array((int) $zone_id, $this->_zones_taxable);
    }
}
