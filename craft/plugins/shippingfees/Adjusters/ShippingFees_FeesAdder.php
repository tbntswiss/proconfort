<?php
namespace Commerce\Adjusters;

use Craft\Commerce_LineItemModel;
use Craft\Commerce_OrderAdjustmentModel;
use Craft\Commerce_OrderModel;

class ShippingFees_FeesAdder implements Commerce_AdjusterInterface
{
    public function adjust(Commerce_OrderModel &$order, array $lineItems = []) {
        $adjustments = [];

        $anti_ttc_factor = 1;
        $tva_rate = 20;
        $prepa = 3;

        if (!empty($order->shippingAddress)) {
            // Prepa adjustment
            $prep = new Commerce_OrderAdjustmentModel();
            $prep->type = \Craft\Craft::t('Parcel preparation');
            $prep->name = \Craft\Craft::t('Parcel preparation');
            $prep->description = \Craft\Craft::t('Parcel preparation fees');
            $prep->amount = +$prepa;
            $prep->orderId = $order->id;
            $prep->optionsJson = ['lineItemsAffected' => null];
            $prep->included = false;

            $adjustments[] = $prep;

            $order->baseTax = $order->baseTax + $prepa;

            // VAT adjustment
            $is_taxable = \Craft\craft()->shippingFees_shippingMethods->isOrderTaxable($order);
            $vat_total = 0;

            if ($is_taxable === true) {
                $cart_total = $order->itemTotal + $order->baseShippingCost + $prepa;
                $vat_total = $cart_total * ($tva_rate / 100);
            }

            $vat = new Commerce_OrderAdjustmentModel();
            $vat->type = \Craft\Craft::t('VAT');
            $vat->name = \Craft\Craft::t($is_taxable ? 'VAT' : 'Not applicable VAT');
            $vat->description = \Craft\Craft::t($tva_rate.'%');
            $vat->amount = $vat_total;
            $vat->orderId = $order->id;
            $vat->optionsJson = ['lineItemsAffected' => null];
            $vat->included = false;

            $adjustments[] = $vat;

            $order->baseTax = $order->baseTax + $vat_total;
        }

        return $adjustments;
    }
}
