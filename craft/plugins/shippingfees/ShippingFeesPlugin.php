<?php
namespace Craft;

require_once(__DIR__.'/shippingmethods/ShippingFees_BaseMethod.php');
require_once(__DIR__.'/shippingmethods/ShippingFees_ColissimoDeliveryMethod.php');
require_once(__DIR__.'/shippingmethods/ShippingFees_ChronopostDeliveryMethod.php');
require_once(__DIR__.'/Adjusters/ShippingFees_FeesAdder.php');

class ShippingFeesPlugin extends BasePlugin
{

	function init()
	{
		// Add event listeners here
	}

	function getName()
	{
		return Craft::t('Shipping fees');
	}

	function getVersion()
	{
		return '0.0.1';
	}

	function getDeveloper()
	{
		return 'tbnt.trainee.raph';
	}

	function getDeveloperUrl()
	{
		// To be adjusted
		return '';
	}

	function getDescription()
	{
		return Craft::t('Defines shipping fees related to one or several products from an order.');
	}

	public function getSourceLanguage()
	{
		return 'fr';
	}

	public function getSchemaVersion()
	{
		return '0.0.1';
	}

	// Shipping methods registering hook
	public function commerce_registerShippingMethods()
	{
		$order = craft()->commerce_cart->getCart();

		// ShippingFeesPlugin::log($order);

		if ($order && $order->shippingAddress) {
			return [
				new ShippingFees_baseMethod($order),
				new ShippingFees_ColissimoDeliveryMethod($order),
				new ShippingFees_ChronopostDeliveryMethod($order),
			];
		}
	}

	// Adjusters registering hook
	public function commerce_registerOrderAdjusters()
	{
		return [
			602 => new \Commerce\Adjusters\ShippingFees_FeesAdder
		];
	}
}