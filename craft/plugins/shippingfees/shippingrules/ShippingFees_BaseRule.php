<?php
namespace Craft;

use Commerce\Interfaces\ShippingRule;

require_once(__DIR__.'/../ShippingFeesPlugin.php');

class ShippingFees_BaseRule implements ShippingRule
{

    private $_order;

    public function __construct(\Craft\Commerce_OrderModel $order) {
        $this->_order = $order;
    }

    public function matchOrder(\Craft\Commerce_OrderModel $order)
    {
        return craft()->shippingFees_shippingMethods->methodMatchesOrder($this->_order, 'chronopost') === false
            && craft()->shippingFees_shippingMethods->methodMatchesOrder($this->_order, 'colissimo') === false;

        // if (craft()->shippingFees_shippingMethods->methodMatchesOrder($this->_order) === false)
        //     return true;
    }

    public function getIsEnabled()
    {
        return true;
    }

    public function getOptions()
    {
        return []; // TODO: define options
    }

    public function getPercentageRate()
    {
        return 0;
    }

    public function getPerItemRate()
    {
        return 0;
    }

    public function getWeightRate()
    {
        return 0;
    }

    public function getBaseRate()
    {
        if ($this->_order->itemTotal >= 400)
            return 0;

        return 45; // Default fee of 45€ for non-EU countries
    }

    public function getMaxRate()
    {
        return 0;
    }

    public function getMinRate()
    {
        return 0;
    }

    public function getDescription()
    {
        return \Craft\Craft::t('Shipping to countries out the european union');
    }

}