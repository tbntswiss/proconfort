<?php
namespace Craft;

use Commerce\Interfaces\ShippingRule;

class ShippingFees_ColissimoRule implements ShippingRule
{

    private $_order;
    private $_shippingMethod = 'colissimo';

    public function __construct(\Craft\Commerce_OrderModel $order) {
        $this->_order = $order;
    }

    /**
     * Is this rule a match on the order? If false is returned, the shipping engine tries the next rule.
     *
     * @return bool
     */
    public function matchOrder(\Craft\Commerce_OrderModel $order) {
        return craft()->shippingFees_shippingMethods->methodMatchesOrder($this->_order, $this->_shippingMethod);
    }

    /**
     * Is this shipping rule enabled for listing and selection
     *
     * @return bool
     */
    public function getIsEnabled() {
        return true;
    }

    /**
     * Stores this data as json on the orders shipping adjustment.
     *
     * @return mixed
     */
    public function getOptions() {
        // TODO
        return [];
    }

    /**
     * Returns the percentage rate that is multiplied per line item subtotal.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getPercentageRate() {
        return 0;
    }

    /**
     * Returns the flat rate that is multiplied per qty.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getPerItemRate() {
        return 0;
    }

    /**
     * Returns the rate that is multiplied by the line item's weight.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getWeightRate() {
        return 0;
    }

    /**
     * Returns a base shipping cost. This is added at the order level.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getBaseRate() {
        if ($this->_order->itemTotal >= 400)
            return 0;

        return craft()->shippingFees_shippingMethods->getShippingFee($this->_order, $this->_shippingMethod);
    }

    /**
     * Returns a max cost this rule should ever apply.
     * If the total of your rates as applied to the order are greater than this, the baseShippingCost
     * on the order is modified to meet this max rate.
     *
     * @return float
     */
    public function getMaxRate() {
        // TODO fetch max cost
        // $max = xxx;
        // return $max;

        return 0;
    }

    /**
     * Returns a min cost this rule should have applied.
     * If the total of your rates as applied to the order are less than this, the baseShippingCost
     * on the order is modified to meet this min rate.
     * Zero will not make any changes.
     *
     * @return float
     */
    public function getMinRate() {
        // TODO fetch min cost
        // $min = xxx;
        // return $min;

        return 0;
    }

    /**
     * Returns a description of the rates applied by this rule;
     * Zero will not make any changes.
     *
     * @return string
     */
    public function getDescription() {
        // Translate to 'standard delivery mode'
        return Craft::t('Standard shipping method');
    }
}