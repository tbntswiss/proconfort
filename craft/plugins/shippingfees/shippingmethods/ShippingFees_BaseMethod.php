<?php
namespace Craft;

use Commerce\Interfaces\ShippingMethod;

require_once(__DIR__.'/../shippingrules/ShippingFees_BaseRule.php');

class ShippingFees_BaseMethod implements ShippingMethod {

    private $_order;

    public function __construct(\Craft\Commerce_OrderModel $order) {
        $this->_order = $order;
    }

    public function getType()
    {
        return 'ShippingFees_Base';
    }

    public function getId()
    {
        // Returning null since this method isn't directly managed by Craft Commerce
        return null;
    }

    public function getName()
    {
        // TODO: change once the plugin is deemed stable
        return Craft::t('Shipping to countries out the european union');
    }

    public function getHandle()
    {
        return 'base';
    }

    public function getCpEditUrl()
    {
        // '' => No Cp section for this shipping method atm
        return '';
    }

    public function getRules()
    {
        return [new ShippingFees_BaseRule($this->_order)];
    }

    public function getIsEnabled()
    {
        return true;
    }
}