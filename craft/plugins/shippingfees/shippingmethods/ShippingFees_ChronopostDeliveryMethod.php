<?php
namespace Craft;

use Commerce\Interfaces\ShippingMethod;

require_once(__DIR__.'/../shippingrules/ShippingFees_ChronopostRule.php');

class ShippingFees_ChronopostDeliveryMethod implements ShippingMethod
{

    private $_order;

    public function __construct(\Craft\Commerce_OrderModel $order) {
        $this->_order = $order;
    }

/**
     * Returns the type of Shipping Method. This might be the name of the plugin or provider.
     * The core shipping methods have type: `Custom`. This is shown in the control panel only.
     *
     * @return string
     */
    public function getType() {
        return 'ShippingFees_Chronopost';
    }

    /**
     * Returns the ID of this Shipping Method, if it is managed by Craft Commerce.
     *
     * @return int|null The shipping method ID, or null if it is not managed by Craft Commerce
     */
    public function getId() {
        return null;
    }

    /**
     * Returns the name of this Shipping Method as displayed to the customer and in the control panel.
     *
     * @return string
     */
    public function getName() {
        return 'Chronopost';
    }

    /**
     * Returns the unique handle of this Shipping Method.
     *
     * @return string
     */
    public function getHandle() {
        return 'chronopost';
    }

    /**
     * Returns the control panel URL to manage this method and it's rules.
     * An empty string will result in no link.
     *
     * @return string
     */
    public function getCpEditUrl() {
        return '';
    }

    /**
     * Returns an array of rules that meet the `ShippingRules` interface.
     *
     * @return \Commerce\Interfaces\ShippingRules[] The array of ShippingRules
     */
    public function getRules() {
        return [new ShippingFees_ChronopostRule($this->_order)];
    }

    /**
     * Is this shipping method enabled for listing and selection by customers.
     *
     * @return bool
     */
    public function getIsEnabled() {
        return true;
    }

}