<?php
namespace Craft;

class DefaultOrderStatus_StatusRecord extends BaseRecord
{
	public function getTableName()
	{
		return 'defaultorderstatus_statuses';
	}

	protected function defineAttributes()
	{
		return array(
			'enabled' => [
				AttributeType::Bool,
				'default' => 0,
				'required' => true
			],
		);
	}

	public function defineRelations()
	{
		return [
			'paymentMethod' => [
				static::BELONGS_TO,
				'Commerce_PaymentMethodRecord',
				'required' => true,
				//'unique' => true,
				'onDelete' => static::CASCADE,
				'onUpdate' => static::CASCADE
			],
			'orderStatus' => [
				static::BELONGS_TO,
				'Commerce_OrderStatusRecord',
				'required' => true,
				'onDelete' => static::CASCADE,
				'onUpdate' => static::CASCADE
			]
		];
	}

		// Add a custom validator for this record
	public function rules()
	{
		$rules = parent::rules();

		$rules[] = ['paymentMethod', 'validatePaymentMethod'];

		return $rules;
	}

	/**
	 * Indicates whether the status is associated with a payment method that already has another status
	 *
	 * @return bool
	 */
	public function validatePaymentMethod()
	{
		$isUnique = true;

		// Temporary uniqueness check (must be defined in another way)
		$statuses = craft()->defaultOrderStatus_status->getAllStatuses();

		foreach ($statuses as $status ) {
			// Is there already a status for that paymentMethod ?
			if ($status->paymentMethodId == $this->paymentMethodId) {
				$message = Craft::t('Un status a déjà été attribué pour cette méthode de paiment');
				$this->addError('paymentMethod', $message);
				$isUnique = false;
				break;
			}
		}

		return $isUnique;
	}
}