<?php
namespace Craft;

class DefaultOrderStatus_StatusService extends BaseApplicationComponent
{

	/**
	 * @param DefaultOrderStatus_StatusModel $model
	 *
	 * @return bool
	 * @throws Exception
	 * @throws \CDbException
	 * @throws \Exception
	 */
	public function saveStatus(DefaultOrderStatus_StatusModel $model)
	{
		if ($model->id)
		{
			$record = DefaultOrderStatus_StatusRecord::model()->findById($model->id);

			if (!$record)
			{
				throw new Exception(Craft::t('No status exists with the ID “{id}”', ['id' => $model->id]));
			}
		}
		else
		{
			$record = new DefaultOrderStatus_StatusRecord();
		}

		$record->paymentMethodId = $model->paymentMethodId;
		$record->orderStatusId = $model->orderStatusId;
		$record->enabled = $model->enabled;

		$record->validate();
		$model->addErrors($record->getErrors());

		//die(json_encode($model->getAllErrors()));

		if (!$model->hasErrors())
		{
			// Save it!
			$record->save(false);

			// Now that we have a record ID, save it on the model
			$model->id = $record->id;

			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	* @param int $id
	*
	* @return DefaultOrderStatus_StatusModel|null
	*/
	public function getStatusById($id)
	{
		$result = DefaultOrderStatus_StatusRecord::model()->findById($id);

		if ($result)
		{
			return DefaultOrderStatus_StatusModel::populateModel($result);
		}

		return null;
	}

	/**
	 * @param array|\CDbCriteria $criteria
	 *
	 * @return DefaultOrderStatus_StatusModel[]
	 */
	public function getAllStatuses($criteria = [])
	{
		$criteria = new \CDbCriteria();
		$criteria->condition = 'isArchived=0';
		$criteria->order = 't.id ASC'; // t = yii primary table (defaultOrderStatus) alias

		// Fetching statuses associated with payment methods that aren't archived
		$statusRecords = DefaultOrderStatus_StatusRecord::model()->with('paymentMethod')->findAll($criteria);
		//$test = DefaultOrderStatus_StatusModel::populateModels($statusRecords);

		// $ids = [];
		// foreach ($test as $plz) {
		// 	$ids[] = $plz->id;
		// }

		// die(json_encode($ids));

		return DefaultOrderStatus_StatusModel::populateModels($statusRecords);
	}

	/**
	 * @param int $id
	 *
	 * @throws \CDbException
	 */
	public function deleteStatusById($id)
	{
		DefaultOrderStatus_StatusRecord::model()->deleteByPk($id);
	}

	// Useful additions

	/**
	 * @param array|\CDbCriteria $criteria
	 *
	 * @return Commerce_PaymentMethodModel[]
	 */
	public function getAllPaymentMethods($criteria = [])
	{
		return craft()->commerce_paymentMethods->getAllFrontEndPaymentMethods();
	}

	/**
	 * @param array|\CDbCriteria $criteria
	 *
	 * @return Commerce_PaymentMethodModel[]
	 */
	public function getStatusWithPaymentMethodId($id)
	{
		$criteria = new \CDbCriteria();
		$criteria->condition = "paymentMethod.id=$id";

		// Fetching statuses associated with a paymentMethod that has the id specified by parameter
		$statusRecord = DefaultOrderStatus_StatusRecord::model()->with('paymentMethod')->find($criteria);

		return DefaultOrderStatus_StatusModel::populateModel($statusRecord);
	}
}