<?php
namespace Craft;

class DefaultOrderStatus_StatusController extends BaseController
{

	/**
	 * @param array $variables
	 * @throws HttpException
	 */
	public function actionIndex(array $variables = [])
	{
		$variables['statuses'] = craft()->defaultOrderStatus_status->getAllStatuses();

		$this->renderTemplate('defaultorderstatus/index', $variables);
	}

	/**
	 * @throws Exception
	 * @throws HttpException
	 * @throws \Exception
	 */
	public function actionSave()
	{
		$this->requirePostRequest();

		$id = craft()->request->getPost('statusId');
		$status = craft()->defaultOrderStatus_status->getStatusById($id);

		if (!$status)
		{
			$status = new DefaultOrderStatus_StatusModel();
		}

		$status->paymentMethodId = craft()->request->getPost('paymentMethodId');
		$status->orderStatusId = craft()->request->getPost('orderStatusId');
		$status->enabled = craft()->request->getPost('enabled');

		// Save it
		if (craft()->defaultOrderStatus_status->saveStatus($status)) {
			craft()->userSession->setNotice(Craft::t('Status saved.'));
			$this->redirectToPostedUrl(); // Triggers actionIndex
		} else {
			craft()->userSession->setError(Craft::t('Couldn’t save status.'));
		}
	}

	public function actionDelete()
	{
		$this->requirePostRequest();

		$statusID = craft()->request->getParam('statusId');

		if ($statusID !== null ) {
			craft()->defaultOrderStatus_status->deleteStatusById($statusID);
		}
	}

	public function actionEdit(array $variables = []) {
		// die(json_encode($this->getActionParams()));
		// DefaultOrderStatusPlugin::log($this->getActionParams() === null);

		if (empty($variables['status'])) {
			if (!empty($variables['id'])) {
				$id = $variables['id'];
				$variables['status'] = craft()->defaultOrderStatus_status->getStatusById($id);

				//die($variables['status']->getPaymentMethod()->name);

				if (!$variables['status']) {
					throw new HttpException(404);
				}
			} else {
				$variables['status'] = new DefaultOrderStatus_StatusModel();
			}
		}

		// CP view title
		if (!empty($variables['orderStatusId'])) {
			$variables['title'] = Craft::t('Edition de status par défaut');
		} else {
			$variables['title'] = Craft::t('Nouveau status par défaut');
		}

		// Fetch and format data to create dropdowns in the CP
		// Just like Commerce_OrderStatusesController and CommerceVariable.php
		$paymentMethods = craft()->commerce_paymentMethods->getAllFrontEndPaymentMethods();
		$variables['paymentMethods'] = \CHtml::listData($paymentMethods, 'id', 'name');

		$orderStatuses = craft()->commerce_orderStatuses->getAllOrderStatuses();
		$variables['orderStatuses'] = \CHtml::listData($orderStatuses, 'id', 'name');

		// die(json_encode($variables['paymentMethods']));

		$this->renderTemplate('defaultorderstatus/_edit', $variables);
	}
}