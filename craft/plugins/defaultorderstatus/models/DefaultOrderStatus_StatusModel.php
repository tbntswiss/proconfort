<?php
namespace Craft;

class DefaultOrderStatus_StatusModel extends BaseModel
{

	/**
	 * @return string
	 */
	public function getCpEditUrl()
	{
		return UrlHelper::getCpUrl('defaultorderstatus/' . $this->id);
	}

	/**
	 * @return Commerce_PaymentMethodModel|null
	 */
	public function getPaymentMethod()
	{
		return craft()->commerce_paymentMethods->getPaymentMethodById($this->getAttribute('paymentMethodId'));
	}

	/**
	 * @return Commerce_OrderStatusModel|null
	 */
	public function getOrderStatus()
	{
		return craft()->commerce_orderStatuses->getOrderStatusById($this->orderStatusId);
	}

	protected function defineAttributes()
	{
		return array(
			'id' => AttributeType::Number,
			'paymentMethodId' => [
				'type'      => AttributeType::Number,
				'required'  => true
			],
			'orderStatusId' => [
				'type'      => AttributeType::Number,
				'required'  => true
			],
			'enabled' => [
				'type'      => AttributeType::Bool,
				'required'  => true
			],
		);
	}
}