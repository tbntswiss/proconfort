<?php
namespace Craft;

class DefaultOrderStatusPlugin extends BasePlugin
{

	function init()
	{
		// Listen to the event occurring before an orders are saved
		craft()->on('commerce_orders.onBeforeOrderComplete', [$this, 'overWriteDefaultStatus']);
	}

	/*
	* Overwrites an order default status with another status chosen by the user
	*/
	function overWriteDefaultStatus(Event $event) {
		$order = $event->params['order'];

		// Get the payment method used to pass the order
		$paymentMethodId = $order->paymentMethodId;

		// Get the status set to override the default one in Commerce plugin
		$overridingStatus = craft()->defaultOrderStatus_status->getStatusWithPaymentMethodId($paymentMethodId);

		// Making sure the overriding status has been enabled
		if ($overridingStatus->enabled) {
			$overridingStatusId = $overridingStatus->getOrderStatus()->id;

			// Setting the status on the order
			$order->orderStatusId = $overridingStatusId;

			//$logString = "PaymentMethod was: $order->paymentMethod->name | satus was: $order->orderStatus->id | new status will be: $overridingStatus->getOrderStatus()->name ($overridingStatusId)";

			//DefaultOrderStatusPlugin::log($logString);
		}
	}

	function getName()
	{
		return Craft::t('Status de commande par défaut');
	}

	function getVersion()
	{
		return '0.0.1';
	}

	function getDeveloper()
	{
		return 'tbnt.trainee.raph';
	}

	function getDeveloperUrl()
	{
		// To be adjusted
		return '';
	}

	function getDescription()
	{
		return Craft::t('Défini le status par défaut d\'une commande en fonction du mode de paiement utilisé.');
	}

	public function getSourceLanguage()
	{
		return 'fr';
	}

	public function getSchemaVersion()
	{
		return '0.0.1';
	}

	// Shows the plugin in the CP side pane
	public function hasCpSection()
	{
		return true;
	}

	public function registerCpRoutes()
	{
		return array(
			// Mapping logic: '<CP URL>' => [<template URL> | <['action' => <controller action request URL>]>]
			'defaultorderstatus'             => ['action' => 'defaultOrderStatus/status/index'],
			'defaultorderstatus/new'         => ['action' => 'defaultOrderStatus/status/edit'],
			'defaultorderstatus/(?P<id>\d+)' => ['action' => 'defaultOrderStatus/status/edit']
		);
	}

}