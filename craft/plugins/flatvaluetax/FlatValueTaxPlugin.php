<?php
namespace Craft;

require_once(__DIR__.'/Adjusters/FlatValueTax_FlatTaxAdder.php');

class FlatValueTaxPlugin extends BasePlugin
{

	function init()
	{
		// Add event listeners here
	}

	function getName()
	{
		 return Craft::t('Taxe à valeur arbitraire');
	}

	function getVersion()
	{
		return '0.0.1';
	}

	function getDeveloper()
	{
		return 'tbnt.trainee.raph';
	}

	function getDeveloperUrl()
	{
		// To be adjusted
		return '';
	}

	function getDescription()
	{
		return Craft::t('Défini une taxe à valeur arbitraire qui sera appliquée à toute les commandes.');
	}

	public function getSourceLanguage()
	{
		return 'fr';
	}

	public function getSchemaVersion()
	{
		return '0.0.1';
	}

	// Shows the plugin in the CP side pane
	public function hasCpSection()
	{
		return true;
	}

	// Adjusters registering hook
	public function commerce_registerOrderAdjusters()
	{
		return [
			601 => new \commerce\Adjusters\FlatValueTax_FlatTaxAdder
		];
	}

	public function registerCpRoutes()
	{
		return array(
			// Mapping logic: '<CP URL>' => [<template URL> | <['action' => <controllerActionRequest URL>]>]
			'flatvaluetax'             => ['action' => 'flatValueTax/taxes/index'],
			'flatvaluetax/new'         => ['action' => 'flatValueTax/taxes/edit'],
			'flatvaluetax/(?P<id>\d+)' => ['action' => 'FlatValueTax/taxes/edit']
		);
	}
}
