<?php
namespace Craft;

class FlatValueTax_TaxesController extends BaseController
{
	/**
	 * @param array $variables
	 * @throws HttpException
	 */
	public function actionIndex(array $variables = [])
	{
		$variables['taxes'] = craft()->flatValueTax_taxes->getAllTaxes();

		$this->renderTemplate('flatvaluetax/index', $variables);
	}

	/**
	 * @throws Exception
	 * @throws HttpException
	 * @throws \Exception
	 */
	public function actionSave()
	{
		$this->requirePostRequest();

		$id = craft()->request->getPost('taxId');
		$tax = craft()->flatValueTax_taxes->getTaxById($id);

		if (!$tax)
		{
			$tax = new FlatValueTax_TaxModel();
		}

		$tax->name = craft()->request->getPost('name');
		$tax->amount = craft()->request->getPost('amount');
		$tax->description = craft()->request->getPost('description');
		$tax->enabled = craft()->request->getPost('enabled');

		if (craft()->flatValueTax_taxes->saveTax($tax)) {
			craft()->userSession->setNotice(Craft::t('Taxe enregistrée.'));
			$this->redirectToPostedUrl(); // Triggers actionIndex
		} else {
			craft()->userSession->setError(Craft::t('La taxe n\'a pas pu être enregistrée.'));
		}
	}

	public function actionDelete()
	{
		$this->requirePostRequest();

		$taxID = craft()->request->getParam('taxId');

		if ($taxID !== null ) {
			craft()->flatValueTax_taxes->deleteTaxById($taxID);
		}
	}

	public function actionEdit(array $variables = []) {
		if (empty($variables['tax'])) {
			if (!empty($variables['id'])) {
				$id = $variables['id'];
				$variables['tax'] = craft()->flatValueTax_taxes->getTaxById($id);

				if (!$variables['tax']) {
					throw new HttpException(404);
				}
			} else {
				$variables['tax'] = new FlatValueTax_TaxModel();
			}
		}

		// CP view title
		if (!empty($variables['orderStatusId'])) {
			$variables['title'] = Craft::t('Edition de la taxe');
		} else {
			$variables['title'] = Craft::t('Nouvelle taxe');
		}

		$this->renderTemplate('flatvaluetax/_edit', $variables);
	}
}