<?php
namespace Craft;

class FlatValueTax_TaxRecord extends BaseRecord
{

    /**
     * @return string
     */
    public function getTableName()
    {
        return 'flatvaluetax_taxes';
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return array(
            'name' => [AttributeType::String, 'required' => true],
            'amount' => [
                AttributeType::Number => array('decimals' => 3),
                'required'  => true
            ],
            'description' => [AttributeType::String, 'required' => true],
            'enabled' => [
                AttributeType::Bool,
                'default' => 0,
                'required' => true
            ],
        );
    }
}