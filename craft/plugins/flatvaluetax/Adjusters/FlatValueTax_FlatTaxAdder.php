<?php
namespace Commerce\Adjusters;
use Craft\Commerce_LineItemModel;
use Craft\Commerce_OrderAdjustmentModel;
use Craft\Commerce_OrderModel;

class FlatValueTax_FlatTaxAdder implements Commerce_AdjusterInterface
{

	public function adjust(Commerce_OrderModel &$order, array $lineItems = []){
		// Adjustments that will be used on the order
		$adjustments = [];

		// The total amount of taxes that will be applied to the order
		$taxesTotal = 0;

		$taxes = \Craft\craft()->flatValueTax_taxes->getAllTaxes();

		foreach ($taxes as $tax) {
			// Is the tax allowed to be applied to orders ?
			if ($tax->enabled == 1) {
				$flatTaxAdder = new Commerce_OrderAdjustmentModel();

				$flatTaxAdder->type = \Craft\Craft::t('Taxe');
				$flatTaxAdder->name = \Craft\Craft::t($tax->name);
				$flatTaxAdder->description = \Craft\Craft::t($tax->description);
				$flatTaxAdder->amount = $tax->amount;
				$flatTaxAdder->orderId = $order->id;
				$flatTaxAdder->optionsJson = ['lineItemsAffected' => null];
				$flatTaxAdder->included = false;

				$adjustments[] = $flatTaxAdder;
				$taxesTotal += $tax->amount;
			}
		}

		// Update the order taxes total
		$order->baseTax = $order->baseTax + $taxesTotal;

		return $adjustments;
	}
}
