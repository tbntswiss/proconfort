<?php
namespace Craft;

class FlatValueTax_TaxesService extends BaseApplicationComponent
{

    /**
     * @param FlatValueTax_TaxModel $model
     *
     * @return bool
     * @throws Exception
     * @throws \CDbException
     * @throws \Exception
     */
    public function saveTax(FlatValueTax_TaxModel $model)
    {
        if ($model->id)
        {
            $record = FlatValueTax_TaxRecord::model()->findById($model->id);

            if (!$record)
            {
                throw new Exception(Craft::t('No tax exists with the ID “{id}”', ['id' => $model->id]));
            }
        }
        else
        {
            $record = new FlatValueTax_TaxRecord();
        }

        $record->name = $model->name;
        $record->amount = $model->amount;
        $record->description = $model->description;
        $record->enabled = $model->enabled;

        $record->validate();
        $model->addErrors($record->getErrors());

        //die(json_encode($model->getAllErrors()));

        if (!$model->hasErrors())
        {
            // Save it!
            $record->save(false);

            // Now that we have a record ID, save it on the model
            $model->id = $record->id;

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
    * @param int $id
    *
    * @return FlatValueTax_TaxModel|null
    */
    public function getTaxById($id)
    {
        $result = FlatValueTax_TaxRecord::model()->findById($id);

        if ($result)
        {
            return FlatValueTax_TaxModel::populateModel($result);
        }

        return null;
    }

    /**
     * @param array|\CDbCriteria $criteria
     *
     * @return FlatValueTax_TaxModel[]
     */
    public function getAllTaxes()
    {
        $statusRecords = FlatValueTax_TaxRecord::model()->findAll();
        // FlatValueTaxPlugin::log(json_encode(FlatValueTax_TaxModel::populateModels($statusRecords)));

        return FlatValueTax_TaxModel::populateModels($statusRecords);
    }

    /**
     * @param int $id
     *
     * @throws \CDbException
     */
    public function deleteTaxById($id)
    {
        FlatValueTax_TaxRecord::model()->deleteByPk($id);
    }
}