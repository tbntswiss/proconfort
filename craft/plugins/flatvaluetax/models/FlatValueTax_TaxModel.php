<?php
namespace Craft;

class FlatValueTax_TaxModel extends BaseModel
{

    /**
     * @return string
     */
    public function getCpEditUrl()
    {
        return UrlHelper::getCpUrl('flatvaluetax/' . $this->id);
    }

    /**
     * @return array
     */
    protected function defineAttributes()
    {
        return array(
            'id' => AttributeType::Number,
            'name' => [AttributeType::String, 'required' => true],
            'amount' => [
                AttributeType::Number => array('decimals' => 3),
                'required'  => true
            ],
            'description' => [AttributeType::String, 'required' => true],
            'enabled' => [
                AttributeType::Bool,
                'default' => 0,
                'required' => true
            ],
        );
    }
}