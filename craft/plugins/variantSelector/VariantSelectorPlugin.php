<?php
namespace Craft;

class VariantSelectorPlugin extends BasePlugin
{

	function init()
	{
		// Add event listeners here
	}

	function getName()
	{
		 return Craft::t('Selectionneur de variantes');
	}

	function getVersion()
	{
		return '0.0.1';
	}

	function getDeveloper()
	{
		return 'tbnt.trainee.raph';
	}

	function getDeveloperUrl()
	{
		// To be adjusted
		return '';
	}

	function getDescription()
	{
		return Craft::t('Permet de sélectionner les variantes d\'un produit en particulier afin de les ajouter au panier.');
	}

	public function getSourceLanguage()
	{
		return 'fr';
	}

	public function getSchemaVersion()
	{
		return '0.0.1';
	}

}