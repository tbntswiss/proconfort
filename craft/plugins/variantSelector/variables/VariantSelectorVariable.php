<?php
namespace Craft;

class VariantSelectorVariable
{
	public function getUniqueVariantAttributeValues($fieldName, $variants, $onlyInStock=false) {
		$addedIds = array();
		$sizes = array();

		foreach($variants as $variant) {
			$outOfStock = (($variant->stock <= 0) && ($variant->unlimitedStock == false));

			if (!$onlyInStock || ($onlyInStock && !$outOfStock)) {
				if (isset($variant->{$fieldName}[0])) {
					if (array_search($variant->{$fieldName}[0]->slug, $addedIds, true) === false) {
						$sizes[] = $variant->{$fieldName}[0];
						$addedIds[] = $variant->{$fieldName}[0]->slug;
					}
				}
			}
		}

		return $sizes;
	}
}