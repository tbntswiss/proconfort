<?php
return [
	// ------------------ General ------------------
	'Submit' => 'Envoyer',
	'For assistance, please contact us' => 'Pour toute assistance, merci de nous contacter',
	'Tel' => 'Tél',
	'here' => 'ici',
	'Switzerland' => 'Suisse',


	// --------------- Control panel ---------------
	// Shipping methods
	'Free Shipping' => 'Livraison gratuite',
	'All Countries, free shipping.' => 'Livraison gratuite dans tous les pays.',


	// -------------------- Home --------------------
	'We develop orthopaedic equipment for horses prepared for high level competition.' => 'Nous  développons du matériel orthopédique pour chevaux.',
	'Our ligament and tendon supports and stable blankets are designed according to a medical approach, approved by equine veterinarian clinics and rehabilitation centres for horses.' => 'Nos supports ligamentaires et tendineux ainsi que nos couvertures d’écurie sont élaborés selon une approche médicale, sous la caution de cliniques vétérinaires équines et centres de remise en forme pour chevaux.',
	'Shop now' => 'Découvir	',
	'Scroll down' => 'Scroll',

	'Buy this product' => 'je suis intéressé',

	'see more' => 'lire',

	// Homepage product 1
	'Orthopedic horse blanket 400 gr' => 'Couvertures orthopédique 400 gr',
	'With its polyester exterior and polycoton lining, winter Pro-Confor blanket is perfectly suitable for low temperatures.' => 'La couverture Pro-Confort hiver est composée d\'un extérieur en polyester rip-top, d\'une isolation polyester et d\'une doublure en poly-coton.',
	'2 Adjustable crossed straps at front' => '2 Sangles réglables croisées sur le poitrail',
	'Inter-changeable Velcro for name tag or sponsor ‘s logo' => 'Double velcro pour badge nominatif interchangeable',
	'Removable Orthopedic foam' => 'Mousse orthopédique amovible',
	'Horse blanket 1200D breathable ripstop' => 'Couverture de box 1200D ripstop, respirant',
	'2 adjustable under belly straps' => '2 Sangles croisées réglables au poitrail.',


	// Homepage product 2
	'Horse boots Topaze' => 'Support ligamentaire Topaze',
	'Support for tendons and ligaments' => 'Maintiennent les tendons et ligaments',
	'Even distribution of pressure' => 'Répartissent uniformément la pression',
	'Temperature regulation' => 'Régulent la température',
	'Improved elimination of lactic acid' => 'Facilitent l’élimination de l’acide lactique',
	'Stimulation of blood oxygenation' => 'Stimulent l’oxygénation sanguine',
	'Ligament support wrap Topaze.' => 'Support ligamentaire Topaze',
	'Ultra light  natural perforated foam' => 'Mousse ultra légère, perforée, anti bactérienne, anti acariens.',
	'Removable parts for easy wash.' => 'Déhoussable et lavable en machine.',


	'Read' => 'lire',

	// -------------------- Nav --------------------
	'Products' => 'Produits',
	'Cart' => 'Panier',
	'About' => 'A propos',
	'News' => 'Actualités',
	'Contact' => 'Contact',

	// ----------- Static page titles -------------
	'My orders' => 'Mes commandes',
	'My addresses' => 'Mes adresses',
	'Address management' => 'Gestion d\'adresse',

	// --------------- User account ---------------
	'My account' => 'Mon compte',
	'Register' => 'Créez votre compte',
	'Username' => 'Nom d\'utilisateur',
	'Username or email' => 'Nom d\'utilisateur ou email',
	'Password' => 'Mot de passe',
	'Remember me' => 'Se rappeler de moi',
	'Login' => 'Connexion',
	'Forget your password?' => 'Mot de passe oublié ?',
	'An email has been sent to your address' => 'Un email a été envoyé à votre adresse',
	'New Password' => 'Nouveau mot de passe',
	'Save' => 'Sauvegarder',

	// _includes/form
	'Alternative Phone' => 'Téléphone alternatif',
	'State' => 'Canton/État/Province',

	// customer/addresses/edit
	'Save Address' => 'Enregistrer l\'adresse',


	// ------------------- News -------------------
	'Latest' => 'Dernier article',
	'Next post' => 'Nouveau post',
	'Share this article' => 'Partager notre article',
	'Share this product' => 'Partager notre produit',

	// ------------------- Footer -------------------
	'Pro-Confort research team has developed a line of products'
	.' that will help recuperation of all horses in all disciplines.' =>
	'L\'équipe de recherche Pro-Confort a développé une gamme de produits'
	.' qui aident a la récupération de tous les chevaux dans toutes les disciplines.',
	'Company' => 'Entreprise',
	'Facebook page' => 'Page facebook',
	'Legal Mentions' => 'Mentions Légales',
	'Partners' => 'Partenaires',
	'Distributors' => 'Distributeurs',
	'Ambassadors' => 'Ambassadeurs',
	'Technicity' => 'Technicité',
	'Delivery' => 'Livraisons',
	'Return' => 'Retour',
	'Store Paris' => 'Magasin à Paris',
	'Store Petit-Lancy' => 'Magasin au Petit-Lancy',
	'Languages' => 'Langues',
	'English' => 'Anglais',
	'French' => 'Français',


	// ------------------- News -------------------
	'This field cannot be blank' => 'Ce champ ne peut pas être vide',
	'Firstname and lastname' => 'Prénom et nom',

	// ----------------- Thank you page -----------------
	'You can review your order by following' => 'Vous pouvez consulter votre commande en suivant',
	'this link' => 'ce lien',

	// ------------------ Product pages ------------------
	// Details
	'in stock' => 'en stock',
	'out of stock' => 'en rupture stock',
	'product-patent-icon' => 'patent-fr.png',
	'Shipping costs are not included, please click' => 'Frais de port non inclus, veuillez cliquer',
	'to check them' => 'pour les consulter',
	'Members' => 'Membres',
	'Size' => 'Taille',
	'incl. taxes' => 'TTC',
	'excl. taxes' => 'HT',

	// Pagination
	'First Page' => 'Première page',
	'Next Page' => 'Page suivante',
	'Previous Page' => 'Page précédente',
	'Last Page' => 'Dernière page',


	// ------------------ Orders ------------------
	// Details
	'Attention' => 'A l\'attention de',
	'Order' => 'Commande',
	'The order' => 'La commande',
	'received' => 'reçue',
	'Thanks for making this purchase' => 'Merci d\'avoir passé commande',
	'Order overview' => 'Aperçu de la commande',
	'Delivery address' => 'Adresse de livraison',
	'Billing address' => 'Adresse de facturation',
	'Shipping method' => 'Mode de livraison',
	'Payment method' => 'Mode de paiement',
	'Order total' => 'Total de la commande',
	'Subtotal' => 'Sous-total',
	'For more information' => 'Pour plus d\'informations',
	'on your order' => 'sur votre commande',
	'Visit this page' => 'Visitez cette page',
	'Ordered product' => 'Produit commandé',
	'Ordered products' => 'Produits commandés',
	'The following products' => 'Les produits suivants',
	'The following product' => 'Le produit suivant',
	'from your order' => 'de votre commande',
	'passed on the' => 'passée le',
	'Credit Card' => 'Carte de crédit',
	'Wire transfert' => 'Virement bancaire',

	// Greetings
	'Hello' => 'Bonjour',
	'Kind regards' => 'Cordialement',

	// Shipment
	'have been sent' => 'ont été envoyés',
	'has been sent' => 'a été envoyé',
	'product sent' => 'produit envoyé',

	// Refund
	'refunded' => 'remboursée',
	'The sum of' => 'La somme de',
	'has been refunded' => 'a été remboursée',
	'according to the payment method used to pass this order' => 'selon la méthode de paiement utilisée pour passer cette commande',
	'on the refund' => 'sur le remboursement',

	// Cancellation
	'cancelled' => 'annulée',
	'has been cancelled' => 'a été annulée',
	'on the cancellation' => 'sur l\'annulation',

	// Awaiting payment
	'Awaiting payment' => 'En attente de paiement',
	'Your order will be processed as soon as we receive your payment.' => 'Votre commande sera traitée dès la réception de votre paiement.',


	// ---------------- Checkout ----------------
	// _includes/nav
	'My Orders' => 'Mes commandes',
	'My Addresses' => 'Mes adresses',
	'Logout' => 'Déconnexion',

	'Checkout' => 'Continuer',


	// Cart
	'Product' => 'Produit',
	'Qty' => 'Quantité',
	'Price' => 'Prix',
	'My Note' => 'Ma note',
	'Empty the Cart' => 'Vider le panier',
	'Your cart is empty.' => 'Votre panier est vide',
	'Apply' => 'Appliquer',
	'Coupon' => 'le coupon',
	'Total Price' => 'Prix total',
	'You have no items in your cart, add some on the' => ' Vous n\'avez pas d\'article dans votre panier. Voir la',
	'products page' => 'page produit',

	// side cart
	'My cart' => 'Mon panier',


	// Index
	'Already Registered ?' => 'Déjà inscrit(e) ?',
	'Guest Checkout' => 'Passer commande en tant qu\'invité(e)',
	'Continue as Guest' => 'Continuer en tant qu\'invité(e)',

	// meter
	'Start Order' => 'Début de la commande',
	'Addresses' => 'Adresses',
	'Shipping' => 'Livraison',
	'Payment' => 'Paiement',

	// Addresses checkout step
	'Billing Address' => 'Adresse de facturation',
	'Use same address for billing' => 'Utiliser la même adresse pour la facturation',
	'Confirm addresses' => 'Confirmer les adresses',

	// _includes/fieldset
	'Edit' => 'Editer',
	'New Shipping Address' => 'Nouvelle adresse de livraison',
	'New Billing Address' => 'Nouvelle adresse de facturation',

	// Shipping checkout step
	'Shipping Method' => 'Mode de livraison',
	'Select' => 'Sélectionnez',
	'Select Shipping Method' => 'Sélectionnez un mode de livraison',
	'Shipping Address' => 'Adresse de livraison',
	'No shipping address selected.' => 'Pas d\'adresse de livraison Sélectionnée',
	'No billing address selected.' => 'Pas d\'adresse de facturation Sélectionnée',
	'Please select a shipping method' => 'Veuillez sélectionner une mode de livraison',
	'No shipping method is available for your region.' => 'Aucun mode de livraison n\'est disponible pour votre région.',
	'For more information regarding orders, please contact our customer service by following this' => 'Pour plus d\'informations concernant les commandes, veuillez contacter notre service clientèle en suivant ce',

	// Payment checkout step
	'Payment' => 'Paiement',
	'Select a payment method' => 'Sélectionnez une méthode de paiement',
	'Order Review' => 'Résumé de la commande',
	'No shipping method has been selected' => 'Aucun mode de livraison n\'a été sélectionné',
	'Choose a shipping method' => 'Sélectionnez un mode de livraison',
	'Card Holder' => 'Détenteur de la carte',
	'Card Number' => 'Numéro de carte',
	'Card Expiry Date' => 'Date d\'expiration de la carte',
	'Pay' => 'Payer',
	'Now' => 'maintenant',
	'Please note duties and taxes are not included for non EU countries' => ' Veuillez noter que les droits et taxes ne sont pas inclus dans les pays d’importation hors UE',

	// _includes/paymentInformation/base
	'Account management' => 'Titulaire du compte',
	'Account number' => 'Numéro de compte',
	'BIC (SWIFT-Code)' => 'BIC (Code SWIFT)',
	'Address your payment using the following information.'
	.' Your order will be processed as soon we receive your payment.' =>
	'Adressez votre paiement en utilisant les informations ci-dessous.'
	.' Votre commande sera traitée dès réception du paiement.',


	// Redirection
	'Redirecting...' => 'Redirection...',
	'Redirecting to products...' => 'Redirection sur les produits...',


	// --------------- Customer ---------------

	// addresses/index
	'Manage Addresses' => 'Gestion des adresses',
	'Add New Address' => 'Ajouter une nouvelle adressse',
	'delete' => 'supprimer',
	'Manage Addresses' => 'Gestion des adresses',

	// Index
	'Total Quantity' => 'Quantité totale',
	'Item Total' => 'Nombre d\'article total',
	'Item' => 'Article',
	'Items' => 'Articles',
	'No payment methods available.' => 'Pas de method de paiement valable',
	'Payment currency' => 'Devise the paiement',

	// Order
	'Taxes incl' => 'Taxes incl',
	'Amount Paid' => 'Montant payé',
	'Receipt' => 'Reçu',
	'Download' => 'Télécharger',
	'Unit Price' => 'Prix unitaire',
	'Quantity' => 'Quantité',
	'SKU' => 'UGS',
	'Details' => 'Détails',
	'Select a color' => 'Sélectionnez une couleur',
	'Select a size' => 'Sélectionnez une taille',
	'Select members' => 'Sélectionnez des membres',



	// PDF
	'Receipt' => 'Facture',
	'Receipt date' => 'Date de la facture',
	'Receipt for Order' => 'Reçu de la commande',
	'Item Total' => 'Sous-total',
	'Seller' => 'Vendeur',
	'Buyer' => 'Acheteur',
	'VAT number' => 'Numéro de TVA',
	'Terms and payment conditions' => 'Termes et conditions de paiement',
	'Due date' => 'Date d\'échéance',
	'Banking coordinates' => 'Coordonnées bancaires',

	// --------- Commerce Stock Notifier ---------
	// CP
	'Notify Email' => 'Email(s) notifié(s)',

	'The email address(es) that should be notified when the stock gets below the threshold.'
	. ' Separate multiple emails with commas.' =>
	'Adresse(s) email à notifier lorsque '
	. 'la quantité de stock d\'un produit atteint le seuil défini ci-dessous. Séparez les adresses email avec une virgule.',

	'Low Stock Threshold' => 'Seuil de notification',
	'The stock amount that should trigger a notification.' => 'La quantité de stock qui déclanchera la notification',

	// ------------- Custom Plugins -------------
	// Flat Value Tax
	'Flat Value Tax' => 'Taxe Forfaitaire',
	'Define a flat value tax for a specific zone.' => 'Défini une taxe forfaitaire pour une zone spécifique.',
	'Tax name' => 'Nom de la taxe',
	'The name of the tax that will be displayed to the users' => 'le nom de la taxe qui sera affichée pour les utilisateurs',
	'Tax value' => 'Valeur de la taxe',
	'The amount that will be added to the order total' => 'La somme qui sera ajoutée à la somme totale de la commande',

	// Shipping fees
	// Plugin file
	'Shipping fees' => 'Frais d\'expédition',
	'Defines shipping fees related to one or several products from an order.' => 'Défini les frais d\'expédition relatifs à un ou plusieur produits d\'une commande.',
	// Adjuster file
	'Parcel preparation' => 'Traitement de commande',
	'parcel fee' => 'frais de traitement',
	'Parcel preparation fees' => 'Frais de traitement de commande',
	'VAT' => 'TVA',
	'vat' => 'tva',
	'Not applicable VAT' => 'TVA non applicable',
	'Value-added tax' => 'Taxe sur la valeur ajoutée',
	// Shipping method files
	'Shipping to countries out the european union' => 'Livraison vers pays hors de l\'union européenne',
	// Shipping rule files
	'Shipping fees for countries out the European union' => 'Frais d\'envoi pour pays hors de l\'union européenne.',
	'Standard shipping method' => 'Mode de livraison standard',
	'Express shipping method' => 'Mode de livraison express',


	// ------------- Errors & message -------------
	'JavaScript is not enabled!' => 'Javascript est déactivé',
	'Page not found' => 'Page inexistante',
	'The page you were looking for couldn\'t be found.' => 'La page n\'est plus disponible',


	// ------------- Product notification (to be deleted ?) -------------
	'This product is unavailable with those criterias' => 'Ce produit n\'est pas disponible avec ces options',
];
?>