<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

$localhost = 'localhost';
$folder_name = '/proconfort/';

// uncomment the following for 'sharing'
// $localhost = '192.168.1.x';

return array(
	// --------------------------------------------------------------------------
	// Global settings (applied regardless of the current environment)
	// --------------------------------------------------------------------------
	'*' => array(
		// Default Week Start Day (0 = Sunday, 1 = Monday...)
		'basePath' => __DIR__.'/../../',

		// Default Week Start Day (0 = Sunday, 1 = Monday...)
		'defaultWeekStartDay' => 1,

		// Enable CSRF Protection (recommended, will be enabled by default in Craft 3)
		'enableCsrfProtection' => true,

		// Whether "index.php" should be visible in URLs (true, false, "auto")
		'omitScriptNameInUrls' => 'auto',

		// Control Panel trigger word
		'cpTrigger' => 'admin',

		// Template used to render the reset password page
		'setPasswordPath' => array(
			'fr' => 'resetpassword',
			'en' => 'resetpassword'
		),

		// Template that should be used to render the page displayed after a password reset
		'setPasswordSuccessPath' => array(
			'fr' => 'login',
			'en' => 'login'
		),
	),

	// --------------------------------------------------------------------------
	// Development environment
	// --------------------------------------------------------------------------
	$localhost => array(
		// Dev Mode (see https://craftcms.com/support/dev-mode)
		'devMode' => true,

		// Base site URL
		'siteUrl' => [
			'fr' => 'http://' . $localhost . $folder_name . 'fr',
			'en' => 'http://' . $localhost . $folder_name . 'en',
		],

		// Base resource URL for files such as twig templates, js or css
		'baseResourceUrl' => 'http://' . $localhost . $folder_name,

		// Environment-specific variables (see https://craftcms.com/docs/multi-environment-configs#environment-specific-variables)
		'environmentVariables' => array(
			'basePath' => '../',
			'baseUrl'  => 'http://' . $localhost . $folder_name,
		)
	),

	// --------------------------------------------------------------------------
	// Production environment
	// --------------------------------------------------------------------------
	'tbnt.digital' => array(
		// Base site URL
		'siteUrl' => [
			'fr' => 'http://tbnt.digital/data/proconfort/fr',
			'en' => 'http://tbnt.digital/data/proconfort/en',
		],

		// Base resource URL for files such as twig templates, js or css
		'baseResourceUrl' => 'http://tbnt.digital/data/proconfort/',

		// Environment-specific variables (see https://craftcms.com/docs/multi-environment-configs#environment-specific-variables)
		'environmentVariables' => array(
			'basePath' => '/data/proconfort/',
			'baseUrl'  => 'http://tbnt.digital/data/proconfort/',
		)
	)
);
