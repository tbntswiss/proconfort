var elixir = require('laravel-elixir');

elixir.config.js.folder = '../../';
elixir.config.js.outputFolder = '../';

elixir.config.css.folder = '../../';
elixir.config.css.outputFolder = '../';

elixir.config.css.less.folder = '../../';
elixir.config.css.sass.folder = '../../';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix)
{
	// Styles
	mix.sass(
		'assets/css/app.scss',
		'assets/build/css/custom.css'
	);

	mix.styles(
		[
			'node_modules/nprogress/nprogress.css',
			'node_modules/owl.carousel/dist/assets/owl.carousel.css',
			'node_modules/owl.carousel/dist/assets/owl.theme.default.css',
			'assets/build/css/custom.css',
		],
		'public/app/css/app.css'
	);

	// Scripts
	mix.webpack(
		[
			'assets/js/main.js',
		],
		'assets/build/js/custom.js'
	);

	mix.scripts(
		[
			'node_modules/jquery/dist/jquery.js',
			'node_modules/lodash/lodash.min.js',
			'node_modules/gsap/src/uncompressed/TweenMax.js',
			'node_modules/gsap/src/uncompressed/plugins/ScrollToPlugin.js',
			'node_modules/nprogress/nprogress.js',
			'node_modules/wow.js/dist/wow.js',
			'assets/js/functions.js',
			'node_modules/owl.carousel/dist/owl.carousel.js',
			'assets/build/js/custom.js'
		],
		'public/app/js/app.js'
	);
});
