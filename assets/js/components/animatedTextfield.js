const label = $('.inputAnimation label');
const input = $('.inputAnimation input');

input.each(function() {
	test($(this));
});

function test(input) {
	if (input.val() === '') {
		input.prev('label').removeClass('active');
	} else {
		input.prev('label').addClass('active');
	}
}

input.focus(function() {
	$(this).prev('label').addClass('active');
	//console.log($(this));
});

input.blur(function() {
	test($(this));
});

input.on('input', function() {
	test($(this));
});