'use strict';

import InputNumberFilter from '../modules/inputNumberFilter';
import AlertBuilder from '../modules/alertBuilder';

// Handles user interactions with the side cart

const cart = $('#sideCart');
const cartCount = $('.cartCount');
const sideCartCount = $('#sideCartCount');

var timeoutID;

function delayRequest(callBack) {
	// Clears the current delayed request, if any
	if(timeoutID) {
		clearRequest();
		console.log('reset #' + timeoutID);
	}

	// initiates a request to be called in x [MS]
	timeoutID = window.setTimeout(callBack, 350); // 550 -> np arrowUP

	console.log('initiate #' + timeoutID);
}

function clearRequest() {
	window.clearTimeout(timeoutID);
}


// ------------------------------------------------
// Open cart
// ------------------------------------------------
$('#openCart').click(function(event) {
	event.preventDefault();
	cart.addClass('open');
});


// ------------------------------------------------
// Close cart
// ------------------------------------------------
$('#closeCart').click(function() {
	cart.removeClass('open');
});

// Close the cart whenever the user clicks anywhere on a page
$('main').click(function() {
	cart.removeClass('open');
});


// ------------------------------------------------
// Remove items
// ------------------------------------------------

function removeItem() {
	$(this).parents('div.product').remove();

	var removeItemForm = $(this).next();
	prepareItemUpdate(removeItemForm)
}

$('.removeItem').click(removeItem);


// ------------------------------------------------
// Product quantity
// ------------------------------------------------

function prepareItemUpdate(form) {
	// Serialize cart data to pass it to the AJAX request
	var formData = form.serializeArray();

	// Craft commerce action controller URL
	// Always sits at index 0 given the current HTML structure
	const actionUrl = formData[0].value;

	// URL that points to the subtotal template
	var subtotalUrl = form.data('subtotal-url');

	updateCart(actionUrl, formData, subtotalUrl);
}

function handleInput(inputElement, cartNeedsUpdate) {
	if (!InputNumberFilter.validate(inputElement, inputElement.val())) {
		// Force the input value to match the min attribute in case the user enters "", 0 or e
		inputElement.val(inputElement.attr('min'));

		// TODO: Display UI errors from here
	}

	if (cartNeedsUpdate) {
		// Update the cart
		delayRequest(function() {
			var quantityForm = inputElement.parents('form');
			prepareItemUpdate(quantityForm);
		});
	}
}

function validateField(numberInput, newValue, callback) {
	if (InputNumberFilter.validate(numberInput, newValue)) {
		// Update the input value
		numberInput.val(newValue);

		if (callback) {
			callback(numberInput);
		}
	}
}

function decrementQuantity(event, button, callback) {
	event.preventDefault();

	var numberInput = button.next();
	var newValue = numberInput.val() - 1;

	validateField(numberInput, newValue, callback);
}

function incrementQuantity(event, button, callback) {
	event.preventDefault();

	var numberInput = button.prev();
	var newValue = parseInt(numberInput.val(), 10) + 1;

	validateField(numberInput, newValue, callback);
}

// Input event handling from the cart
$('.sideCart.spinner').on('input', function() {
	handleInput($(this), true);
});

// Input event handling from the product page
$('.productPage.spinner').on('input', function() {
	handleInput($(this), false);
});

// Increment and decrement from the cart
$('.updateItemQuantity .sideCart.decrementButton').click(function(event) {
	decrementQuantity(event, $(this), function(numberInput) {
		delayRequest(function() {
			// Update the cart
			var quantityForm = numberInput.parents('form');
			prepareItemUpdate(quantityForm);
		});
	});
});

$('.updateItemQuantity .sideCart.incrementButton').click(function(event) {
	incrementQuantity(event, $(this), function(numberInput) {
		delayRequest(function() {
			// Update the cart
			var quantityForm = numberInput.parents('form');
			prepareItemUpdate(quantityForm);
		});
	});
});

// Increment and decrement from the product page
$('.updateItemQuantity .productPage.decrementButton').click(function(event) {
	decrementQuantity(event, $(this));
});

$('.updateItemQuantity .productPage.incrementButton').click(function(event) {
	incrementQuantity(event, $(this));
});


// ------------------------------------------------
// AJAX
// ------------------------------------------------

// Updates the cart via AJAX using the the passed url, form data and
// callback function to be executed if the request is successful
function updateCart(url, formData, subtotalUrl, newItemData) {
	$.ajax({
		url: url,
		method: "POST",
		data: formData,
		success: function(msg) {
			if (msg.success) {
				console.log(msg);

				// Are we adding an item to the cart ?
				if (newItemData) {
					// Update the cart according to the item
					updateItem(newItemData);
				}

				// Get the number of different products in the cart
				var totalLineItems = msg.cart.totalLineItems;

				// Make sure the cart has lineItems before displaying the item subtotal
				if (totalLineItems > 0) {
					$('#emptyCartMessage').addClass('hidden');
					$('#cartFooter').removeClass('hidden');

					cartCount.html(totalLineItems);
					cartCount.removeClass('hidden');

					updateCartSubtotal(subtotalUrl);
				} else {
					$('#emptyCartMessage').removeClass('hidden');
					$('#cartFooter').addClass('hidden');

					cartCount.addClass('hidden');
				}

				sideCartCount.html(totalLineItems);

				if (cart.hasClass('open') === false) cart.addClass('open');
			} else {
				console.log('[Error]:' + msg.error);
			}
		},
		error: function(msg) {
			console.log('error');
			console.log(msg);
		}
	});
}

// Fetches and displays the formatted cart item subtotal via AJAX
function updateCartSubtotal(templateUrl) {
	$.ajax({
		method: 'GET',
		url: templateUrl,
		success: function(result) {
			// Update the cart item subtotal
			$('#cartFooter #cartTotal span').html(result);
		},
		error: function(result) {
			console.log('get error');
			console.log(result);
		},
	});
}

// ------------------------------------------------
// Product page controls
// ------------------------------------------------

$('#productParams #addToCart').click(function(event) {
	event.preventDefault();

	var addToCartForm = $(this).parents('#addToCartForm');

	var formData = addToCartForm.serializeArray();

	// URL that points to the subtotal template
	var subtotalUrl = addToCartForm.data('subtotal-url');

	console.log(formData);

	// Get the purchasable id, template url and quantity for the product that has been added
	var newItemData = {
		// /!\ The following indexes have to match the html structure
		quantity: formData[2].value,
		purchasableId: formData[3].value,
		templateUrl: addToCartForm.data('product-template-url')
	};

	console.log(newItemData);

	updateCart('commerce/cart/updateCart', formData, subtotalUrl, newItemData);
});

function updateItem(newItemData) {
	let productIsInCart = false;

	// Holds the product which info will be updated
	let currentProduct;

	var products = $('#cartProducts .product');

	console.log(newItemData);

	// Make sure the item doesn't already exist in the cart
	if (products.length) {
		products.each(function() {
			console.log($(this).data('purchasable-id'));

			if ($(this).data('purchasable-id') == newItemData.purchasableId) {
				console.log('product already exists');
				currentProduct = $(this);
				productIsInCart = true;
				return false; // break out of .each
			}
		});
	}

	// Add the item if it's not in the cart
	if (!productIsInCart) {
		addCartItem(newItemData.templateUrl);
	} else {
		const spinner = currentProduct.find('.sideCart.spinner');
		const oldValue = parseInt(spinner.val(), 10);
		const newValue = parseInt(newItemData.quantity, 10);

		// update the item count
		spinner.val(oldValue + newValue);
	}
}

function addCartItem(templateUrl) {
	$.ajax({
		method: 'GET',
		url: templateUrl,
		success: function(result) {
			// Div containing line items (products)
			var cartProducts = $('#cartProducts');

			// Add event handlers to the new item controls
			let productHtml = $(result);
			addEventListeners(productHtml);

			// Update the cart with the new item
			cartProducts.append(productHtml);
		},
		error: function(result) {
			console.log('get error');
			console.log(result);
		},
	});
}

function addEventListeners(productHtml) {
	// Item removal
	$(productHtml).find('.removeItem').click(removeItem);

	// Input
	$(productHtml).find('.sideCart.spinner').on('input', function() {
		handleInput($(this), true);
	});

	// Decrement
	$(productHtml).find('.sideCart.decrementButton').click(function(event) {
		decrementQuantity(event, $(this), function(numberInput) {
			delayRequest(function() {
				// Update the cart
			var quantityForm = numberInput.parents('form');
			prepareItemUpdate(quantityForm);
			});
		});
	});

	// increment
	$(productHtml).find('.sideCart.incrementButton').click(function(event) {
		incrementQuantity(event, $(this), function(numberInput) {
			delayRequest(function() {
				// Update the cart
				var quantityForm = numberInput.parents('form');
				prepareItemUpdate(quantityForm);
			});
		});
	});
}
