// Handles marker behaviour on various events

const marker = $('.marker');

function hoverIn() {
	let cardText = $(this).parents('.card').first();
	cardText.css('visibility', 'visible');
}

function hoverOut() {
	let cardText = $(this).parents('.card').first();
	cardText.css('visibility', 'hidden');
}

marker.hover(hoverIn, hoverOut);