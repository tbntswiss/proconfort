'use strict';

let el = {};

let _version = 0;

const init = () =>
{
	initDOM();
	initEvents();
	initAjax();
};

const initDOM = () =>
{
	el = {};
};

const initEvents = () =>
{

};

const initAjax = () =>
{
	_Ajax.setPrefix('ajax');
	_Ajax.setVersion(_version);
};

export default
{
	init,
};
