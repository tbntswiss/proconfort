'use strict';

let el = {};

let _wow = null;

const init = () =>
{
	initWow();
};

const initWow = () =>
{
	if (_Utils.isNavigator.ieEdge() === true)
		return;



	_wow = new WOW({
		boxClass: 'wow-anim', // animated element css class (default is wow)
		animateClass: 'animated', // animation css class (default is animated)
		offset: 0, // distance to the element when triggering the animation (default is 0)
		mobile: false, // trigger animations on mobile devices (default is true)
		live: true, // act on asynchronously loaded content (default is true)
		callback: function(box)
		{
			// the callback is fired every time an animation is started
			// the argument that is passed in is the DOM node being animated

			// console.log('animated', box);
		},
		scrollContainer: null, // optional scroll container selector, otherwise use window
	});

	_wow.init();

};

export default
{
	init,
};
