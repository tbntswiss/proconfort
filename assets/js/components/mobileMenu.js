const header = $('header');
const menu = $('#mobileMenu');
const menuBtn = $('#burgerMenu');
const iconStrokes = $('#account g.navbar');

var menuIsOpened = false;

menuBtn.click(function() {
	menuIsOpened = !menuIsOpened;

	menu.toggleClass('open');
	menuBtn.toggleClass('open');

	if (menuIsOpened) {
		if (header.hasClass('landing')) {
			header.removeClass('dark');
			header.addClass('light');
			iconStrokes.attr('stroke', '#191935');
		}
	} else {
		if (header.hasClass('landing')) {
			header.removeClass('light');
			header.addClass('dark');
			iconStrokes.attr('stroke', '#FFFFFF');
		}
	}
});