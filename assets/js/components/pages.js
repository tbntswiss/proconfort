'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();

	animStart();
};

const initDOM = () =>
{
	el = {};
};

const initEvents = () =>
{
	$(document)
		.on('click touch', '[data-prevent]', function(e) { e.preventDefault(); return false; })
		.on('click touch', '[data-go-to-top]', function(e) { scrollToTop(); });
};

const animEnd = () =>
{

};

const animStart = () =>
{

};

const scrollToTop = () =>
{
	TweenLite.to('body', 1, { scrollTo: 0 });
};

export default
{
	init,
	scrollTop: scrollToTop,
};

$(document).on('pjax:send', function() { animStart(); });
$(document).on('pjax:callback', function() { animEnd(); });