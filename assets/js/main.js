'use strict';

// Modules
import _Ajax from './modules/ajax';
import _App from './modules/app';
import _Pjax from './modules/pjax';
import _Utils from './modules/utils';
import InputNumberFilter from './modules/inputNumberFilter';
import AlertBuilder from './modules/alertBuilder';

// Components
import Animate from './components/animate';
import App from './components/app';
import Pages from './components/pages';
import Share from './components/share';
import Marker from './components/marker';
import Cart from './components/cart';
//import Cart from './components/go-top';
import MobileMenu from './components/MobileMenu';
import animatedTextfield from './components/animatedTextfield';

// Pages
import Home from './pages/home';
import Post from './pages/post';
import Product from './pages/product';
import Contact from './pages/contact'

// Make modules accessible
_Utils.access(_Ajax, '_Ajax');
_Utils.access(_App, '_App');
_Utils.access(_Pjax, '_Pjax');
_Utils.access(_Utils, '_Utils');

// Main
$(document).ready(function()
{
	$('.owl-carousel').owlCarousel({
		items: 1,
		lazyLoad: true,
		// autoplay:true,
		// autoplayTimeout:5000,
		// autoplayHoverPause:true,
	});

	_App.init([
		{ page: 'app', js: [App, Animate] }, // , Animate
	]);

	_App.registerJs([
		{ page: 'app', js: [Pages, Share] },

		{ page: 'home', js: [Home] },
		{ page: 'post', js: [Post] },
	]);
});