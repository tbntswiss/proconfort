
'use strict';

let el = {};

const init = () =>
{
	initDOM();
	initEvents();
};

const initDOM = () =>
{
	el = {};
};

const initEvents = () =>
{
	// TweenMax.to("#main .loader-background", 0.7, {opacity: .6});
	// TweenMax.fromTo("#main .home-content", 0.7, { y: -40, autoAlpha:0}, {y:0, autoAlpha:1 });
	$('[data-scroll]').on('click', function(){
		TweenLite.to('html', 1, { scrollTo: '#firstProduct' });
	});
};

export default
{
	init,
};




