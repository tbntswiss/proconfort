'use strict';

import AlertBuilder from '../modules/alertBuilder';

// Get the variant selects
const colorSelect = $('select[name="variantColor"]'),
	sizeSelect = $('select[name="variantSize"]'),
	legSelect = $('select[name="variantLeg"]'),
	foamSelect = $('select[name="variantFoam"]'),
	variantSelect = $('select[name="purchasableId"]');

var _tempDrops = $([colorSelect, sizeSelect, legSelect, foamSelect]);
var _allowCheck = false;

const addToCartForm = $('#addToCartForm');
const addToCartBtn = $('#addToCart');

const numberSipnner = $('#productQuantity');

const paramsNotification = $('#paramsNotification');

const stockSpan = $('#productStock')

// console.log(stockSpan);

function checkDropStauses() {
	_tempDrops.each(function() {
		_allowCheck = $(this).val() !== '';
		return _allowCheck; // Break whenever false is encountered
	});
}

// Holds the variant selects values
var selectedColor = null,
	selectedSize = null,
	selectedLeg = null,
	selectedFoam = null;

// Indicates whether or not the product has variants
var productHasVariants = false;

init();

function init() {
	// If the color select exists, add an on change handler to it
	if (colorSelect.length > 0) {
		productHasVariants = true;
		colorSelect.on('change', function(e) {
			selectedColor = $(this).val();
			checkDropStauses();
			selectVariant(selectedColor, selectedSize, selectedLeg, selectedFoam);
		});
	}

	if (sizeSelect.length > 0) {
		productHasVariants = true;
		sizeSelect.on('change', function(e) {
			selectedSize = $(this).val();
			checkDropStauses();
			selectVariant(selectedColor, selectedSize, selectedLeg, selectedFoam);
		});
	}

	if (legSelect.length > 0) {
		productHasVariants = true;
		legSelect.on('change', function(e) {
			selectedLeg = $(this).val();
			checkDropStauses();
			selectVariant(selectedColor, selectedSize, selectedLeg, selectedFoam);
		});
	}

	if (foamSelect.length > 0) {
		productHasVariants = true;
		foamSelect.on('change', function(e) {
			selectedFoam = $(this).val();
			checkDropStauses();
			selectVariant(selectedColor, selectedSize, selectedLeg, selectedFoam);
		});
	}

	if (!productHasVariants) {
		addToCartBtn.prop('disabled', false);

		const baseUrl = addToCartForm.data('product-base-url');
		const newUrl = baseUrl + addToCartForm.data('default-variant-id');

		addToCartForm.data('product-template-url', newUrl);
	}
}

/**
 * Selects a variant based on the value of the attribute dropdowns
 * String color: the selected color
 * String size: the selected size
 * String leg: the selected leg
 */
function selectVariant(color, size, leg, foam) {
	let variantOptions = variantSelect.find('option');
	let selectedOption = null;

	let notification = null;

	variantSelect.val('');

	variantOptions.each(function(i) {
		let match = true;

		let colorMatched = false,
			sizeMatched = false,
			legMatched = false,
			foamMatched = false;

		let inStock = false;

		// Current option variant specs
		let variantColor = String($(this).data('color'));
		let variantSize = String($(this).data('size'));
		let variantLeg = String($(this).data('leg'));
		let variantFoam = String($(this).data('foam'));

		if (colorSelect.length > 0) {
			if ((variantColor !== "" && variantColor === color) === true) {
				colorMatched = true;
			} else {
				colorMatched = false;
			}

			match = match && colorMatched;
		}

		if (sizeSelect.length > 0){
			if ((variantSize !== "" && variantSize === size) === true) {
				sizeMatched = true;
			} else {
				sizeMatched = false;
			}

			match = match && sizeMatched;
		}

		if (legSelect.length > 0) {
			if ((variantLeg !== "" && variantLeg === leg) === true) {
				legMatched = true;
			} else {
				legMatched = false;
			}

			match = match && legMatched;
		}

		if (foamSelect.length > 0) {
			if ((variantFoam !== "" && variantFoam === foam) === true) {
				foamMatched = true;
			} else {
				foamMatched = false;
			}

			match = match && foamMatched;
		}

		if (match) {
			variantSelect.val($(this).attr('value'));
			selectedOption = $(this);

			updatePrice(selectedOption);

			return false; // break out of loop
		} else {
			if (_allowCheck) {
				notification = {
					type: 'error',
					message: 'This product isn\'t available with those criteria'
				};
			}
		}
	});

	if (selectedOption !== null) {
		// Build the url that allows the sideCart to be updated with products
		const baseUrl = addToCartForm.data('product-base-url');
		const newUrl = baseUrl + selectedOption.attr('value');

		addToCartForm.data('product-template-url', newUrl);

		// Handle authorized quantities for the product
		const minQty = selectedOption.data('min-qty');
		const maxQty = selectedOption.data('max-qty');

		if (minQty) {
			numberSipnner.attr('min', minQty);
			numberSipnner.val(minQty);
		} else {
			numberSipnner.val(1);
		}

		if (maxQty)
			numberSipnner.attr('max', maxQty);

		// Is the product out of stock ?
		if (selectedOption !== null && selectedOption.data('out-of-stock') === true) {
			AlertBuilder.build({
				type: 'error',
				message: 'This product is not in stock'
			});
		} else {
			// Enable button
			addToCartBtn.prop('disabled', false);
		}
	} else {
		if (_allowCheck === true && notification != null){
			AlertBuilder.build(notification);
		}

		// Disable button
		addToCartBtn.prop('disabled', true);
	}
}

/**
 * Updates the price based on the variant that was selected. You'll probably
 * want to do some currency formatting in here, and maybe remove the sale
 * price if it's the same as the full price.
 */
function updatePrice(optionElement) {
	const variantId = optionElement.val();

	// Get the variant price template url to fetch the price in the right format
	let templateUrl = optionElement.parent('#variantSelect').data('price-template-url');
	templateUrl += '?variantId=' + variantId + '&includesTax=1';

	$.ajax({
		method: 'GET',
		url: templateUrl,
		success: function(result) {
			// Update the cart with the new item
			$('#productPrice').html(result);
		},
		error: function(result) {
			// console.log('error');
			// console.log(result);
		},
	});
}