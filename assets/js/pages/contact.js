const nameField = $('#fromName');
const emailField = $('#fromEmail');
const messageField = $('#message')

// Plugins that are 'natively' handled by the plugin
const pluginFields = $([emailField, messageField]);

const errorContainers = $('div.errors');
const redirectUrl = $('#redirectUrl').val();
const submitButton = $('#sendContactFormButton');

const emptyFieldMessage = $('#emptyFieldError').val()

// Indicates whether or not the field name is required to submit the form. Toggle at will...
const requireFieldName = true;

// nameField needs a separate validation since it isn't handled by the plugin
function validateNameField() {
	let fieldIsValid = true;

	if (!nameField.val()) {
		nameField.next('div.errors').removeClass('hidden');
		displayError(nameField, [emptyFieldMessage]);
		fieldIsValid = false;
	}

	return fieldIsValid;
}

function displayError(field, message) {
	const errorContainer = $(field).next('div.errors');

	errorContainer.removeClass('hidden');
	errorContainer.append('<span>' + message[0] + '</span>');
}

function handleErrors(errors) {
	_.forEach(errors, function(errorMessage, field) {
		const culpritField = '#' + field;
		displayError(culpritField, errorMessage);
	});
}

$('#contactForm form').submit(function(ev) {
	// Prevent the form from actually submitting
	ev.preventDefault();

	// Indicates whether the form submission should be submitted
	let allowSubmission = true;

	// Disabled submit button to prevent multiple submissions
	submitButton.attr('disabled', '');

	// Clear errors
	errorContainers.html('');
	errorContainers.addClass('hidden');

	if (requireFieldName) {
		// Allow email and message field errors to be displayed if name field is empty
		allowSubmission = validateNameField() && emailField.val() && messageField.val();
	}

	if (allowSubmission) {
		// Get the post data
		var data = $(this).serialize();

		// Send it to the server
		$.post('actions/contactForm/sendMessage', data, function(response) {
			if (response.success) {
				window.open(redirectUrl, '_self');
			} else {
				// response.error will be an object containing any validation errors that occurred, indexed by field name
				// e.g. response.error.fromName => ['From Name is required']

				// console.log(response.error);
				handleErrors(response.error);

				// Enable the button to allow submissions again
				submitButton.removeAttr('disabled');
			}
		});
	} else {
		pluginFields.each(function() {
			if (!$(this).val()) {
				displayError($(this), [emptyFieldMessage]);
			}
		});

		submitButton.removeAttr('disabled');
	}
});