'use strict';

import Pjax from '../../vendor/pjax/index';

let pjax = null;
let baseUrl = '';
let fullUrl = '';
let referrer = '';
let oldReferrer = '';

const callback = function()
{
	initPjax();
	initEvents();
};

const initPjax = function()
{
	baseUrl = $('head').find('base').attr('href');
	fullUrl = window.location.href;

	pjax = new Pjax({
		elements: '_LetTheEventsDoIt_',
		selectors: ['head title', '#app'],
		debug: false,
	});

	$(document)
		.on('pjax:send', pjaxSend)
		.on('pjax:complete', pjaxComplete)
		.on('pjax:success', pjaxSuccess)
		.on('pjax:error', pjaxError);
};

const initEvents = function()
{
	$(document)
		.on('click', 'a[pjax]', goTo)
		.on('click', '[data-href]', goTo);
};

const pjaxSend = function()
{

};

const pjaxComplete = function()
{
	$(window).off('resize');
	$(document).off('click input keyup keydown mouseenter mouseleave touch touchstart touchmove touchend submit change focus scroll');
	$('*').off('click input keyup keydown mouseenter mouseleave touch touchstart touchmove touchend submit change focus scroll');

	_Utils.removeEvents();

	initEvents();

	$(document).trigger('pjax:callback', { referrer: oldReferrer });
	oldReferrer = referrer;
};

const pjaxSuccess = function()
{

};

const pjaxError = function()
{

};

const goTo = function(e)
{
	e.preventDefault();
	href($(this).attr('href') || $(this).attr('data-href'));
};

const href = function(url, callback)
{
	$(document).trigger('pjax:before');

	referrer = url;
	fullUrl = baseUrl + $.trim(url, '/');

	if (_Utils.isFunc(callback) === true)
		$(document).one('pjax:callback', () => setTimeout(() => callback()));

	if ((_Utils.isBool(callback) === true && callback === true)
		|| _Utils.isNavigator.ie() === true
		|| _Utils.isNavigator.edge() === true)
	{
		pjax.latestChance(fullUrl);
	}
	else {
		pjax.loadUrl(fullUrl, pjax.options);
	}
};

const refresh = function(force, callback)
{
	$(document).one('pjax:callback', () => $(document).trigger('pjax:refresh'));

	href(getCurrentRoute(), force, callback);
};

const getFullUrl = function()
{
	return fullUrl;
};

const getCurrentRoute = function()
{
	return fullUrl.replace(baseUrl, '');
};

export default
{
	href,
	refresh,
	getFullUrl,
	getCurrentRoute,
};

$(document).ready(callback);
