// Validates new input values that are entered by the user
// Returns a boolean indicating whether or not it is safe
// To proceed with AJAX requests using the new values
const validateNumberInput = (inputElement, newValue) =>
{
	const maxValue = parseInt(inputElement.attr('max'), 10);
	const minValue = parseInt(inputElement.attr('min'), 10);

	if (newValue < minValue || newValue > maxValue)
		return false;

	var inputIsValid = true;

	// Making sure there's a value
	if (!newValue) return false;

	// Making sure the input value is numeric
	if (isNaN(newValue)) return false;

	// Making sure the value is higher than 0
	if (parseInt(newValue, 10) < 1) return false;

	return inputIsValid;
};

export default
{
	validate: validateNumberInput,
};