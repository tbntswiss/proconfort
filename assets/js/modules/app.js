'use strict';

let _config = [];

const init = function(config)
{
	callJs(config);
};

const callback = function()
{
	callJs();
};

const isJs = function(page)
{
	return jesuisuncraft.js.indexOf(page) > -1;
};

const registerJs = function(config)
{
	_config = _.concat([], _config, config);

	callJs();
};

const callJs = function(config)
{
	_.map(config || _config, (config) => {
		if (isJs(config.page) === false)
			return;

		if (Array.isArray(config.js) === false)
			config.js = [config.js];

		_.map(config.js, (js) => {
			if (js.init !== undefined) js.init();
			else console.warn('Undefined method "init":', _Utils.getObjectType(js), js);
		});
	});
};

export default
{
	init,
	isJs,
	registerJs,
};

$(document).on('pjax:callback', callback);
