'use strict';

let _prefix = '';
let _version = 0;

const _allErrors = [
	'not_authorized',
	'logged_out',
	'csrf_token',
	'post_too_large',
	'need_reload',
];

const _loggedOutErrors = [
	'logged_out',
	'csrf_token',
];

const _newVersionErrors = [
	'need_reload',
];

const get = (url, options = {}) =>
{
	return execute(url, {}, _.assign(options, { type: 'GET' }));
};

const post = (url, data, options = {}) =>
{
	return execute(url, data, _.assign(options, { type: 'POST' }));
};

const execute = (url, data, options = {}) =>
{
	// Init options
	let ajaxOptions = _.assign({
		url: _.trim([options.prefix || _prefix, url].join('/'), '/'),
		data: data,
		type: 'GET',
		timeout: 60 * 1000,
		cache: false,
	}, options);

	// Set complete callback
	ajaxOptions.complete = () => {
		if (options.loader_error !== true && options.loader === true) _Utils.triggerEvent('ajax.loader.stop');
		if (options.complete) options.complete();
	};

	// Set success callback
	ajaxOptions.success = (response) => {
		if (options.debug === true) console.log('Ajax success:', url, response);
		if (options.success) options.success(response);
	};

	// Set error callback
	ajaxOptions.error = (response) => {
		const errors = getErrors(response);
		const errorsNames = _.intersection(_allErrors, _.keys(errors));

		if (response.status === 0) {
			errorsNames.push('network_lost');

			options.silent = false;
			options.loader_error = true;
			delete options.error;
		}

		if (errorsNames.length !== 0) {
			const errorName = errorsNames[0];

			if (_loggedOutErrors.indexOf(errorName) !== -1) {
				_Utils.triggerEvent('ajax.error.logged_out');
			}
			else if (_newVersionErrors.indexOf(errorName) !== -1) {
				_Utils.triggerEvent('ajax.error.new_version');
			}

			if (options.silent === false && _Trans.get('errors.' + errorName) !== 'errors.' + errorName)
				_Utils.triggerEvent('ajax.error.notification', _Trans.get('errors.' + errorName));

			response.is_general = true;
		}

		if (options.debug === true) console.warn('Ajax error:', url, response);
		if (options.loader_error === true) _Utils.triggerEvent('ajax.loader.stop');
		if (options.error) options.error(response);
	};

	// Set data options
	if (_Utils.isObjectType(data, 'FormData') === true) {
		ajaxOptions.processData = false;
		ajaxOptions.contentType = false;

		// if (ajaxOptions.type === 'POST')
		// 	ajaxOptions.data.append('_token', Laravel.token);

		// ajaxOptions.data.append('lang_code', Laravel.lang.code);
		// ajaxOptions.data.append('version', _version);
	}
	else {
		// if (ajaxOptions.type === 'POST')
		// 	ajaxOptions.data._token = Laravel.token;

		// ajaxOptions.data.lang_code = Laravel.lang.code;
		// ajaxOptions.data.version = _version;
	}

	// Debug
	if (options.debug === true)
		console.debug('Ajax start:', url, ajaxOptions, _Utils.debugFormData(data));

	// Start loader
	if (options.loader === true || options.loader_error === true)
		_Utils.triggerEvent('ajax.loader.start');

	// Execute request
	return $.ajax(ajaxOptions);
};

const getErrors = (error) =>
{
	if (_Utils.isObject(error, true) === false)
		return { error: error };

	if (error.responseJSON !== undefined)
		return error.responseJSON;

	if (error.responseText !== undefined)
		return { error: _Trans.get('errors.unknown') };

	return error;
};

const isResponseSuccess = (response) =>
{
	const errorCode = Number(_Utils.isObject(response) ? response.status: 0);
	return errorCode >= 200 && errorCode < 300;
};

const isResponseError = (response) =>
{
	return isResponseSuccess(response) === false;
};

const setPrefix = (prefix) =>
{
	_prefix = prefix;
};

const setVersion = (version) =>
{
	_version = version;
};

export default
{
	get,
	post,
	isResponseSuccess,
	isResponseError,
	setPrefix,
	setVersion,
};
