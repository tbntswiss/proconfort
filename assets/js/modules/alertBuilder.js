let _alertStack = $('.notifications');
const _main = $('main');

// Builds and displays an alert
const buildAlert = (alertData) =>
{
	clearAlertStack();

	// console.log(alertData);

	const alert = $(document.createElement('div'));
	alert.css('text-align', 'center');
	alert.addClass(alertData.type);
	alert.text(alertData.message);

	getAlertStack().prepend(alert);

	alert.click(() => {
		alert.remove();
	});

	fadeAndDie(alert);
};

const fadeAndDie = (target) =>
{
	window.setTimeout(() => {
		TweenLite.to(target, 4, {
			opacity: '0',
			onComplete: (target) => {
				$(target).remove();
			}
		});
	}, 2000);
}

// Creates an alert stack, adds it as the last child of main
const initAlertStack = () =>
{
	_alertStack = $(document.createElement('div'));
	_alertStack.addClass('notifications');

	_main.append(_alertStack);

	return _alertStack;
}

// Gets the currently displayed alert stack div
// Creates and adds one into the page otherwise
const getAlertStack = () =>
{
	// console.log('alert stack request', _alertStack.length > 0);
	return _alertStack.length > 0 ? _alertStack : initAlertStack();
}


const clearAlertStack = () =>
{
	getAlertStack().empty();
}

export default
{
	build: buildAlert,
};